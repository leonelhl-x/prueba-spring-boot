package cl.multicaja.semilla.models.service;

import java.util.List;

import cl.multicaja.semilla.models.entity.Cliente;

public interface ClienteService {
	
	public List<Cliente> findAll();
	public Cliente findClient(Long id);
	public void createClient(Cliente cliente);
	public Cliente findByRut(String rut);
	public void updateClient(Cliente cliente);
	public void deleteClient(Long id);

}
