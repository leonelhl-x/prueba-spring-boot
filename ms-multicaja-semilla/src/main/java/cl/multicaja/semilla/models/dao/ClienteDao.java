package cl.multicaja.semilla.models.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import cl.multicaja.semilla.models.entity.Cliente;

public interface ClienteDao extends CrudRepository<Cliente, Long> {
	
	@Query("Select u from Cliente u where rut = ?1")
	public Cliente findByRut(String rut);
	
}
