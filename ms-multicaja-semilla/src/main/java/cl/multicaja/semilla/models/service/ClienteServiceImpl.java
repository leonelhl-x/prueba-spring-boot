package cl.multicaja.semilla.models.service;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.multicaja.semilla.models.dao.ClienteDao;
import cl.multicaja.semilla.models.entity.Cliente;

@Service
public class ClienteServiceImpl implements ClienteService {
	
	private static final Logger logger = LoggerFactory.getLogger(ClienteServiceImpl.class.getSimpleName());
	
	@Autowired
	private ClienteDao clienteDao;

	@Override
	@Transactional
	public List<Cliente> findAll() {
		return (List<Cliente>)clienteDao.findAll();
	}

	@Override
	public Cliente findClient(Long id) {
		return clienteDao.findById(id).get();
	}

	@Override
	public void createClient(Cliente cliente) {
		clienteDao.save(cliente);
	}

	@Override
	public Cliente findByRut(String rut) {
		return clienteDao.findByRut(rut);
	}

	@Override
	public void updateClient(Cliente cliente) {
		logger.info("Actualizando el cliente: "+ cliente.getNombre());
		clienteDao.save(cliente);
	}

	@Override
	public void deleteClient(Long id) {
		logger.info("Eliminando el cliente con id: "+ id);
		clienteDao.deleteById(id);
	}


}
