package cl.multicaja.semilla.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cl.multicaja.semilla.models.entity.Cliente;
import cl.multicaja.semilla.models.exception.ResourceNotFoundException;
import cl.multicaja.semilla.models.service.ClienteService;

@RestController
@RequestMapping(value = "/clientes")
public class ClienteController {
	
	private static final Logger logger = LoggerFactory.getLogger(ClienteController.class.getSimpleName());
	
	private static final String COD_HTTP200 = "Operacion exitosa";
	
	@Autowired
	private ClienteService clienteService;
	
	@RequestMapping(value="", method=RequestMethod.POST)
	public ResponseEntity<String> crearCliente(@RequestBody Cliente cliente) throws Exception{	
		logger.info("Inicio de servicio guardaCliente");
		try {
			Cliente newCliente = new Cliente();
			clienteService.createClient(newCliente);	}
		catch(Exception e) {
			logger.error(e.getClass().toString() + " " + e.getMessage());
			throw e;
		}		
		logger.info("Fin de servicio guardaCliente");
		return new ResponseEntity<>(COD_HTTP200, HttpStatus.OK);
	}	
	
	@RequestMapping(value="", method=RequestMethod.GET)
	public ResponseEntity<List<Cliente>> obtenerClientes() throws Exception{	
		logger.info("Inicio de servicio obtenerClientes");
		List<Cliente> clientes = null;
		try {
			clientes = clienteService.findAll();
		}
		catch(Exception e) {
			logger.error(e.getClass().toString() + " " + e.getMessage());
			throw e;
		}		
		logger.info("Fin de servicio obtenerClientes");
		return new ResponseEntity<List<Cliente>>(clientes, HttpStatus.OK);
	}	

	@RequestMapping(value="/{id}", method=RequestMethod.GET)	
	public ResponseEntity<Cliente> detalleCliente(@PathVariable("id") Long id) throws Exception, ResourceNotFoundException{	
		Cliente cliente = null;
		logger.info("Inicio de servicio obtenerClienteId");
		try {
			cliente = clienteService.findClient(id);
			if(cliente == null) {
				ResourceNotFoundException e = new ResourceNotFoundException();
				logger.error(e.getClass().toString() + " " + e.getMessage());
				throw e;
			}
		}
		catch(Exception e) {
			logger.error(e.getClass().toString() + " " + e.getMessage());
			throw e;
		}	
		logger.info("Fin de servicio obtenerClienteId");
		return new ResponseEntity<Cliente>(cliente, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	public ResponseEntity<String> actualizaCliente(@PathVariable("id") Long id, @RequestBody Cliente cliente) throws Exception{	
		logger.info("Inicio de servicio actualizaCliente");
		try {
			clienteService.updateClient(cliente);
		}
		catch(Exception e) {
			logger.error(e.getClass().toString() + " " + e.getMessage());
			throw e;
		}	
		logger.info("Fin de servicio actualizaCliente");
		return new ResponseEntity<String>(COD_HTTP200, HttpStatus.OK);
	}	
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<String> deleteClient(@PathVariable("id") Long id) throws Exception{
		logger.info("Inicio de servicio actualizaCliente");
		try {
			clienteService.deleteClient(id);
		}
		catch(Exception e) {
			logger.error(e.getClass().toString() + " " + e.getMessage());
			throw e;
		}		
		logger.info("Fin de servicio actualizaCliente");
		return new ResponseEntity<String>(COD_HTTP200, HttpStatus.OK);
	}

}
